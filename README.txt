CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This is a drupal 8 module for the PublishThis platform. There are lot of modules 
available but no one supporting latest publishthis api's. Using this module 
you can publish content very easily to your site in different content types.

 * For a full description of the module visit:
  https://www.drupal.org/project/publishthis

 * To submit bug reports and feature suggestions, or to track changes visit:
  https://www.drupal.org/project/issues/sitemeta


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 Install PublishThis module as you would normally install a contributed Drupal
 module. Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

  1. Navigate to Administration > Extend and enable the module.
  2. Navigate to Configuration > Publishthis
  3. Enter the publishthis api token obtained from https://publishthis.com
  4. Click 'Publishthis Action' to configure content type for publishthis 
  content.


MAINTAINERS
-----------

  Maintainer for 8.x:
   * Abhishek Kumar - https://www.drupal.org/u/abhishekkumar
